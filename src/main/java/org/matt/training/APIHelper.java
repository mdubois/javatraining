package org.matt.training;

import java.util.*;

public class APIHelper {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    Map<String, Map<String, Map<String, Integer>>> fullMap;
    List<String> sortedProjects;
    Map<String, List<String>> sortedSubProjects;
    Map<String, List<String>> sortedMethods;

    public String[] countAPI(String[] calls) {

        fullMap = new HashMap<String, Map<String, Map<String, Integer>>>();

        sortedProjects = new ArrayList<String>();
        sortedSubProjects = new HashMap<String, List<String>>();
        sortedMethods = new HashMap<String, List<String>>();

        for (String call : calls) {
            processCall(call);
        }

        return buildOutput();
    }

    private void processCall(String call) {
        StringTokenizer st = new StringTokenizer(call, "/");

        // projects
        String project = st.nextToken();
        Map<String, Map<String, Integer>> subProjects = processCallProjects(project);

        // sub projects
        String subProject = st.nextToken();
        Map<String, Integer> methods = processCallSubProjects(project, subProject, subProjects);

        // method
        String method = st.nextToken();
        processCallMethods(project, subProject, method, methods);
    }

    private Map<String, Map<String, Integer>> processCallProjects(String project) {

        if (!sortedProjects.contains(project)) {
            sortedProjects.add(project);
            sortedSubProjects.put(project, new ArrayList<String>());
        }
        Map<String, Map<String, Integer>> subProjects = fullMap.get(project);
        if (subProjects == null) {
            subProjects = new HashMap<String, Map<String, Integer>>();
        }
        fullMap.put(project, subProjects);

        return subProjects;
    }

    private Map<String, Integer> processCallSubProjects(String project, String subProject, Map<String, Map<String, Integer>> subProjects) {

        if (!sortedSubProjects.get(project).contains(subProject)) {
            //sortedSubProjects.put(project, new ArrayList<String>());
            sortedSubProjects.get(project).add(subProject);
        }
        Map<String, Integer> methods = subProjects.get(subProject);
        if (methods == null) {
            methods = new HashMap<String, Integer>();
        }
        subProjects.put(subProject, methods);

        return methods;
    }

    private void processCallMethods(String project, String subProject, String method, Map<String, Integer> methods) {
        if (sortedMethods.get(project + subProject) == null) {
            sortedMethods.put(project + subProject, new ArrayList<String>());
        }
        if (!sortedMethods.get(project + subProject).contains(method)) {
            sortedMethods.get(project + subProject).add(method);
        }
        Integer c = methods.get(method);
        if (c == null) {
            c = new Integer(1);
        } else {
            c = new Integer(c.intValue() + 1);
        }
        methods.put(method, c);
    }

    private String[] buildOutput() {

        List<String> outputList = new ArrayList<String>();
        for (String sortedProject : sortedProjects) {
            outputList.add("--" + sortedProject + " (" + getProjectSum(fullMap, sortedProject) + ")");
            for (String sortedSubProject : sortedSubProjects.get(sortedProject)) {
                outputList.add("----" + sortedSubProject + " (" + getSubProjectSum(fullMap, sortedProject, sortedSubProject) + ")");
                for (String sortedMethod : sortedMethods.get(sortedProject + sortedSubProject)) {
                    outputList.add("------" + sortedMethod + " (" + getMethodSum(fullMap, sortedProject, sortedSubProject, sortedMethod) + ")");
                }
            }
        }

        String[] output = new String[outputList.size()];
        return outputList.toArray(output);
    }

    private static int getMethodSum(Map<String, Map<String, Map<String, Integer>>> bigMap, String project, String subProject, String method) {
        return bigMap.get(project).get(subProject).get(method);
    }

    private static int getSubProjectSum(Map<String, Map<String, Map<String, Integer>>> bigMap, String project, String subProject) {
        int sum = 0;
        for (String method : bigMap.get(project).get(subProject).keySet()) {
            sum += getMethodSum(bigMap, project, subProject, method);
        }
        return sum;
    }

    private static int getProjectSum(Map<String, Map<String, Map<String, Integer>>> bigMap, String project) {
        int sum = 0;
        for (String subProject : bigMap.get(project).keySet()) {
            sum += getSubProjectSum(bigMap, project, subProject);
        }
        return sum;
    }
}

