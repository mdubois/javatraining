package org.matt.training.pluralsight.apicollection;

import org.matt.training.pluralsight.domain.Person;

import java.util.*;

public class MainAPICollection {
    public static void main(String[] args) {

        Person p1 = new Person("Alice", 23);
        Person p2 = new Person("Brian", 56);
        Person p3 = new Person("Chelsea", 46);
        Person p4 = new Person("David", 28);
        Person p5 = new Person("Erica", 37);
        Person p6 = new Person("Francisco", 18);

        City newYork = new City("New York");
        City shangai = new City("Shangai");
        City paris = new City("Paris");


        System.out.println("People (filtered) List:");
        List<Person> peopleList = new ArrayList<>((Arrays.asList(p1, p2, p3, p4, p5, p6)));
        peopleList.removeIf(person -> person.getAge() < 30);
        peopleList.replaceAll(person -> new Person(person.getFirstName().toUpperCase(), person.getAge()));
        peopleList.sort(Comparator.comparing(Person::getAge).reversed());
        peopleList.forEach(System.out::println);

        System.out.println("----------------------------------------------------------------------------------------");

        Map<City, List<Person>> map = new HashMap<>();

        map.putIfAbsent(paris, new ArrayList<>());
        map.get(paris).add(p1);

        map.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p2);
        map.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p3);

        System.out.println("People from Paris: " + map.getOrDefault(paris, Collections.EMPTY_LIST));
        System.out.println("People from New York: " + map.getOrDefault(newYork, Collections.EMPTY_LIST));

        System.out.println("----------------------------------------------------------------------------------------");

        System.out.println("Map 1");

        Map<City, List<Person>> map1 = new HashMap<>();
        map1.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p1);
        map1.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p2);
        map1.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p3);

        // test to check unicity when merging (fail !)
        //map1.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p4);

        map1.forEach((city, p) -> System.out.println(city + " : " + p));


        System.out.println("Map 2");

        Map<City, List<Person>> map2 = new HashMap<>();
        map2.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p4);
        map2.computeIfAbsent(paris,   city -> new ArrayList<>()).add(p5);
        map2.computeIfAbsent(paris,   city -> new ArrayList<>()).add(p6);

        map2.forEach((city, p) -> System.out.println(city + " : " + p));

        map2.forEach(
                (city, people) -> {
                    map1.merge(
                            city, people,
                            (peoplefromMap1, peopleFromMap2) -> {
                                peoplefromMap1.addAll(peopleFromMap2);
                                return peoplefromMap1;
                            }
                    );
                }
        );

        System.out.println("Map 1 merged with Map2");
        map1.forEach((city, p) -> System.out.println(city + " : " + p));



    }
}
