package org.matt.training.pluralsight.collections.mapmerge;

import org.matt.training.pluralsight.domain.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    private static void initMap(Map<City, List<Person>> map, City city, Person person) {
        map.computeIfAbsent(
                city,
                l -> new ArrayList<>())
                .add(person);
    }

    public static void main(String[] args) {
        // Cities
        City paris = new City("Paris");
        City sanFrancisco = new City("San Francisco");
        City seattle = new City("Seattle");

        // Persons
        Person billGates = new Person("Bill", "Gates", 62);
        Person anneHidalgo = new Person("Anne", "Hidalgo", 58);
        Person jeffBezos = new Person("Jeff", "Bezos", 53);
        Person elonMusk = new Person("Elon", "Musk", 46);
        Person patrickCollison = new Person("Patrick", "Collison", 29);
        Person johnCollison = new Person("John", "Collison", 27);
        Person emmanuelMacron = new Person("Emmanuel", "Macron", 39);

        // Maps
        Map<City, List<Person>> map1 = new HashMap<>();
        initMap(map1, seattle, billGates);
        initMap(map1, paris, anneHidalgo);
        initMap(map1, paris, emmanuelMacron);

        Map<City, List<Person>> map2 = new HashMap<>();
        initMap(map2, sanFrancisco, elonMusk);
        initMap(map2, sanFrancisco, patrickCollison);
        initMap(map2, sanFrancisco, johnCollison);
        initMap(map2, sanFrancisco, jeffBezos);
        initMap(map2, paris, emmanuelMacron);

        System.out.println("** after init **");
        System.out.println("map1:");
        map1.forEach( (key, value) -> System.out.println(key + " " + value) );
        System.out.println("map2:");
        map2.forEach( (key, value) -> System.out.println(key + " " + value) );

        System.out.println("Merging:");

        map2.forEach(
                (key, value) ->
                    map1.merge(
                        key, value,
                        (existingPeople, newPeople) -> {
                            existingPeople.addAll(newPeople);
                            return existingPeople;
                        }
                    )
        );

        System.out.println("** after merge **");
        System.out.println("map1:");
        map1.forEach( (key, value) -> System.out.println(key + " " + value) );
        System.out.println("map2:");
        map2.forEach( (key, value) -> System.out.println(key + " " + value) );
    }

}
