package org.matt.training.pluralsight.lambda;

import org.matt.training.pluralsight.domain.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MainComparator {

    public static void main(String ... args) {

        /*
        Comparator<Person> cmpAge = (p1, p2) -> p2.getAge() - p1.getAge();
        Comparator<Person> cmpFirstName = (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName());
        Comparator<Person> cmpLastName = (p1, p2) -> p1.getLastName().compareTo(p2.getLastName());
        */

        /*
        Function<Person, Integer> f1 = p -> p.getAge();
        Function<Person, String> f2 = p -> p.getFirstName();
        Function<Person, String> f3 = p -> p.getLastName();

        Comparator<Person> cmpAge2 = (p1, p2) -> f1.apply(p2) - f1.apply(p1);
        Comparator<Person> cmpPerson = Comparator.comparing(f1);
        */

        Comparator<Person> cmpPersonAge = Comparator.comparing(Person::getAge);
        Comparator<Person> cmpPersoLastName = Comparator.comparing(Person::getLastName);

        //Comparator<Person> cmp = cmpPersonAge.thenComparing(cmpPersoLastName);

        Comparator<Person> cpm = Comparator.comparing(Person::getAge)
                                .thenComparing(Person::getLastName);

        //------

        Person p1 = new Person("Matthieu", "Dubois", 42);
        Person p2 = new Person("Matthieu", "Ricard", 71);
        Person p3 = new Person("Paul", "Ricard", 95);
        Person p4 = new Person("Paul", "Ricard", 12);
        Person p5 = new Person("Robert", "Plant", 65);
        Person p6 = new Person("Robert", "Baratheon", 49);

        List<Person> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);
        persons.add(p6);

        persons.sort(java.util.Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName).thenComparing(Person::getAge));

        for (Person p : persons) {
            System.out.println(p);
        }

        Predicate<String> p = s -> s.length() < 20;

        System.out.println("Predicate ZOE: " + p.test("ZOE"));
        System.out.println("Predicate ANTICONSTITUTIONNELEMENT: " + p.test("ANTICONSTITUTIONNELEMENT"));
    }
}
