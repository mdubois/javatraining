package org.matt.training.pluralsight.mapfilterreduce;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class AssociativeTests {

    private List<Integer> ints;

    public AssociativeTests(List<Integer> ints) {
        super();
        this.ints = ints;
    }

    public List<Integer> getInts() {
        return ints;
    }
    public static void main(String[] args) {

        AssociativeTests t = new AssociativeTests(Arrays.asList(4, 9, 2, 7, 6, 7));

        int oi1 = t.associativeAdd(t.ints);
        System.out.println("associativeAdd: " + oi1);

        int oi2 = t.associativeMax(t.ints);
        System.out.println("associativeMax: " + oi2);
    }

    public int associativeAdd(List<Integer> ints) {
        BinaryOperator<Integer> op1 = (i1, i2) -> i1 + i2;
        return ints.stream().reduce(op1).get();
    }

    public int associativeMax(List<Integer> ints) {
        BinaryOperator<Integer> op2 = (i1, i2) -> Integer.max(i1, i2);
        return ints.stream().reduce(op2).get();
    }

}
