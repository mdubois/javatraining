package org.matt.training.pluralsight.mapfilterreduce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class SumOp {
    public static void main(String[] args) {
        sample1();
        sample2();
    }

    private static void sample1() {
        List<Integer> ints = Arrays.asList(1);
        int sum = 0; // 0 is the identity operator of the BinaryOperator used here (addition)
        BinaryOperator<Integer> op = (i1, i2) -> i1 + i2;
        for (int i : ints) {
            sum = op.apply(sum, i);
        }
        System.out.println(sum);
    }

    private static void sample2() {
        List<Integer> ints = Arrays.asList(-1);
        int sum = 0;
        BinaryOperator<Integer> op = Integer::max;
        for (int i : ints) {
            sum = op.apply(sum, i);
        }
        System.out.println(sum);
    }


}
