package org.matt.training.pluralsight.predicate;

public class Main {

    public static void main(String... args) {
        System.out.println("Start...");

        Predicate<String> p1 = s -> s.length() < 20;
        Predicate<String> p2 = s -> s.length() > 5;

        Predicate<String> p3 = p1.and(p2);

        System.out.println("[p3] hello: " + p3.test("hello"));
        System.out.println("[p3] hello dude: " + p3.test("hello dude"));
        System.out.println("[p3] hello dude, what are you up to today?: " + p3.test("hello dude, what are you up to today?"));

        Predicate<String> p4 = p1.or(p2);

        System.out.println("[p4] hello: " + p4.test("hello"));
        System.out.println("[p4] hello dude: " + p4.test("hello dude"));
        System.out.println("[p4] hello dude, what are you up to today?: " + p4.test("hello dude, what are you up to today?"));

        Predicate<String> p5 = Predicate.isEqualTo("Yes");

        System.out.println("[p5] hello: " + p5.test("hello"));
        System.out.println("[p5] Yes: " + p5.test("Yes"));

        Predicate<Integer> p6 = Predicate.isEqualTo(666);

        System.out.println("[p6] 123456: " + p6.test(123456));
        System.out.println("[p6] 666: " + p6.test(666));

    }
}
