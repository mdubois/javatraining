package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BuildStreams {

    public static void main(String[] args) {
        try {
            buildStreams();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void buildStreams() throws IOException {

        // a Stream from a list
        List<Person> people = new ArrayList<>();
        Stream<Person> stream = people.stream();

        // an empty Stream
        Stream.empty();

        // a singleton Stream
        Stream.of("one");

        // a Stream with several elements
        Stream.of("one", "two", "three");

        // a constant Stream
        Stream.generate(() -> "one");

        // a growing Stream
        Stream.iterate("+", s -> s + "+");

        // a random Stream
        ThreadLocalRandom.current().ints();

        // a Stream on the letters of a String
        IntStream stream2 = "hello".chars();

        // a Stream on a regular expression
        String book = "the content of a whole book!";
        Stream<String> words = Pattern.compile("[^\\p{javaLetter}]").splitAsStream(book);

        // a Stream on the lines of a text file
        Path path = FileSystems.getDefault().getPath("c:\\windows", "win.ini");
        //Path path = Paths.get("c:\\windows\\win.ini");

        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(s -> System.out.println(s));
        }

        // the StreamBuilder pattern
        // build it
        Stream.Builder<String> builder = Stream.builder();
        // chain elements in it
        builder.add("one").add("two").add("three");
        // or accept (but cannot chain on this method)
        builder.accept("four");
        // call the build() method
        Stream<String> stream3 = builder.build();
        // display the stream
        stream3.forEach(System.out::println);

    }
}
