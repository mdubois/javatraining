package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.List;

public class MapFilterReduce {

    public static void main(String[] args) {

        List<Person> persons = PersonFactory.buildPersonList();

        persons.stream()
                .map(p -> p.getAge())
                //.peek(System.out::println)
                .filter(age -> age > 20)
                .forEach(System.out::println);

        persons.stream()
                .filter(p -> p.getAge() > 20)
                .forEach(System.out::println);
    }

}
