package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonFactory {

    static List<Person> buildPersonList() {
        Person p1 = new Person("Alice", 23);
        Person p2 = new Person("Brian", 56);
        Person p3 = new Person("Chelsea", 46);
        Person p4 = new Person("David", 28);
        Person p5 = new Person("Erica", 37);
        Person p6 = new Person("Francisco", 18);

        return new ArrayList<>((Arrays.asList(p1, p2, p3, p4, p5, p6)));
    }
}
