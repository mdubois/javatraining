package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.List;

public class Ranges {

    public static void main(String[] args) {

        List<Person> persons = PersonFactory.buildPersonList();

        persons.stream()
                .peek(p -> System.out.println(" (" + p + ")"))
                .skip(2)
                .limit(3)
                //.map(p -> p.getAge())
                .filter(p -> p.getAge() > 20)
                .forEach(System.out::println);
    }

}
