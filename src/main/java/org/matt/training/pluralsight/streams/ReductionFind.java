package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.List;
import java.util.Optional;

public class ReductionFind {

    public static void main(String[] args) {

        List<Person> persons = PersonFactory.buildPersonList();

        Optional<Person> opt = null;

        opt = persons.stream()
                .filter(p -> p.getAge() >= 30 && p.getAge() < 40)
                .findFirst();
        System.out.println("first: " + (opt.isPresent() ? opt.get() : "none"));

        opt = persons.stream()
                .filter(p -> p.getAge() >= 30 && p.getAge() < 50)
                .findAny();
        System.out.println("any: " + (opt.isPresent() ? opt.get() : "none"));
    }

}
