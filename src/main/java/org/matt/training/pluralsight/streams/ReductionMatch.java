package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.List;

public class ReductionMatch {

    public static void main(String[] args) {

        List<Person> persons = PersonFactory.buildPersonList();

        boolean b = false;

        // at least one is older than 20
        b = persons.stream().anyMatch(p -> p.getAge() > 20);
        System.out.println("anyMatch age > 20 : " + b);

        // all older than 20 yo
        b = persons.stream().allMatch(p -> p.getAge() > 20);
        System.out.println("allMatch age > 20 : " + b);

        // none older than 20 yo
        b = persons.stream().noneMatch(p -> p.getAge() > 20);
        System.out.println("noneMatch age > 20 : " + b);

    }

}
