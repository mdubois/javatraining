package org.matt.training.pluralsight.streams;

import org.matt.training.pluralsight.domain.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public class ReductionReduce {

    public static void main(String[] args) {

        List<Person> persons = PersonFactory.buildPersonList();

        int sumOfAges = persons.stream()
                .mapToInt(Person::getAge)
                .reduce(0, (p1, p2) -> p1 + p2);
        System.out.println("sum: " + sumOfAges);

        int maxOfAges = persons.stream()
                .mapToInt(Person::getAge)
                .reduce(0, (p1, p2) -> Integer.max(p1, p2)); // 0 is the identity element for positive integers too, so that's fine here...
        System.out.println("max: " + maxOfAges);

        OptionalInt opt = persons.stream()
                .mapToInt(Person::getAge)
                .reduce((p1, p2) -> Integer.max(p1, p2)); // 0 is the identity element for positive integers too, so that's fine here...
        System.out.println("max opt: " + (opt.isPresent() ? opt.getAsInt() : "unknown"));

        List<Integer> ages =
                persons.stream().reduce(
                        new ArrayList<Integer>(),
                        (list, p) -> {
                            list.add(p.getAge());
                            return list;
                        },
                        (list1, list2) -> {
                            list1.addAll(list2);
                            return list1;
                        }
                );
        System.out.println("ages = " + ages);
    }
}
