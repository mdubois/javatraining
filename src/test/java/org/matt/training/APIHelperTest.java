package org.matt.training;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class APIHelperTest {

    public APIHelperTest() {
        super();
    }

    APIHelper helper;

    @Before
    public void setUp() {

        helper = new APIHelper();
    }

    @Test
    public void countAPI() {

        String[] calls = new String[]{
                "/project1/subproject1/method1",
                "/project2/subproject1/method1",
                "/project1/subproject1/method1",
                "/project1/subproject2/method1",
                "/project1/subproject1/method2",
                "/project1/subproject2/method1",
                "/project2/subproject1/method1",
                "/project1/subproject2/method1"
        };
        String[] output = helper.countAPI(calls);

        String[] expectedOutput = new String[] {
                "--project1 (6)",
                "----subproject1 (3)",
                "------method1 (2)",
                "------method2 (1)",
                "----subproject2 (3)",
                "------method1 (3)",
                "--project2 (2)",
                "----subproject1 (2)",
                "------method1 (2)"
        };

        Assert.assertArrayEquals(expectedOutput, output);
    }

}