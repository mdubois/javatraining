package org.matt.training.pluralsight.mapfilterreduce;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class AssociativeTestsTest {

    AssociativeTests t;

    @Before
    public void setUp() throws Exception {
        t = new AssociativeTests(Arrays.asList(4, 9, 2, 7, 6, 7));
    }

    @Test
    public void associativeAdd() {
        Assert.assertEquals(35, t.associativeAdd(t.getInts()));
    }

    @Test
    public void associativeMax() {
        Assert.assertEquals(9, t.associativeMax(t.getInts()));
    }
}